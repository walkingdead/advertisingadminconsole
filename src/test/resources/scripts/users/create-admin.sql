INSERT INTO USERS ( LOGIN, PASSWORD, EMAIL)
VALUES ('admin', '$2a$10$0biIX9xckzgMCIrjwvo0XONr3TWrFRkODiiWcSldRKCuOD5plZvEm', 'admin@gmail.com');


INSERT INTO USERS_AUTHORITY (USER_ID, AUTHORITY_ID) VALUES ((SELECT USERS_ID FROM USERS where LOGIN = 'admin'),
                                                            (SELECT AUTHORITY_ID FROM AUTHORITY where AUTHORITY_NAME = 'ADMIN'));

