package com.advertising.console.configuration;


import com.advertising.console.configuration.security.SecurityConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.advertising.console")
@Import(value = {TestPersistenceConfiguration.class, AdvertisingAdminConsoleConfiguration.class, SecurityConfig.class})
public class IntegrationTestConfiguration {
}
