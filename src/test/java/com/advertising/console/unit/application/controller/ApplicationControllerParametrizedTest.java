package com.advertising.console.unit.application.controller;

import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.application.ApplicationController;
import com.advertising.console.application.Application;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.application.enums.AppType;
import com.advertising.console.application.ApplicationService;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junitparams.JUnitParamsRunner.$;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(JUnitParamsRunner.class)
@WebMvcTest(ApplicationController.class)
@WithMockUser
public class ApplicationControllerParametrizedTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @MockBean
    private ApplicationService applicationService;

    private static final Gson gson = new Gson();

    private static final String NAME_REQUIRED = "name is required";
    private static final String CONTENT_TYPE_REQUIRED = "contentTypes are required";

    private static final String NAME_NOT_NULL = "name can't be null";
    private static final String APP_TYPE_NOT_NULL = "appType can't be null";

    private static final User DUMMY_USER = new User("Freddy", "cvbnmio", "freddy@gmail.com",
            Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER)));

    @Test
    @Parameters(method = "parametersForApplicationTest")
    public void createAdoptTest(Application application, ResultMatcher status, String[] errorMessages) throws Exception {
        verifyResults(application, status, errorMessages, post("/app/application/user/1"));
    }


    @Test
    @Parameters(method = "parametersForApplicationTest")
    public void updateAdoptTest(Application application, ResultMatcher status, String[] errorMessages) throws Exception {
        verifyResults(application, status, errorMessages, put("/app/application/user/1"));
    }

    public Object[] parametersForApplicationTest() {
        return $(
                $(new Application(EMPTY, null, null, null), status().isBadRequest(),
                        $string(NAME_REQUIRED)),
                $(new Application("Demo", Sets.newHashSet(), null, null), status().isBadRequest(),
                        $string(CONTENT_TYPE_REQUIRED)),
                $(new Application(null, null, null, null), status().isBadRequest(),
                        $string(NAME_NOT_NULL, APP_TYPE_NOT_NULL)),
                $(new Application("Demo", null, null, null), status().isBadRequest(),
                        $string(APP_TYPE_NOT_NULL)),
                $(new Application("Demo", null, AppType.ANDROID, null), status().isBadRequest(),
                        $string())
        );
    }

    @Test
    @Parameters(method = "parametersForDeleteUserTest")
    public void deleteApplicationTest(MockHttpServletRequestBuilder builder, ResultMatcher resultMatcher) throws Exception {
        this.mvc.perform(builder)
                .andExpect(resultMatcher);
    }

    public Object[] parametersForDeleteUserTest() {
        return $(
                $(delete("/app/application/wer"), status().isInternalServerError()),
                $(delete("/app/application/56"), status().isNoContent())
        );
    }

    private void verifyResults(Application application, ResultMatcher status, String[] errorMessages,
                               MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult result = this.mvc.perform(builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(application)))
                .andExpect(status)
                .andReturn();
        String rawResponse = result.getResponse().getContentAsString();
        List<String> errors = new ArrayList<>();
        if (StringUtils.isNotBlank(rawResponse))
            errors = Arrays.asList(rawResponse.substring(rawResponse.indexOf('[') + 1, rawResponse.lastIndexOf(']')).split(", "));
        assertThat(errors, hasItems(errorMessages));
    }

    public static String[] $string(String... params) {
        return params;
    }
}
