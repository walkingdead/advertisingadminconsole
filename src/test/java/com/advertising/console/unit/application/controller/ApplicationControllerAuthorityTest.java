package com.advertising.console.unit.application.controller;

import com.advertising.console.application.ApplicationController;
import com.advertising.console.application.ApplicationService;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import static junitparams.JUnitParamsRunner.$;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(JUnitParamsRunner.class)
@WebMvcTest(ApplicationController.class)
public class ApplicationControllerAuthorityTest {

    @MockBean
    private ApplicationService applicationService;
    
    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    @Parameters(method = "parametersForUserAuthorityTest")
    public void unauthorizedTest(MockHttpServletRequestBuilder builder) throws Exception {
        this.mvc.perform(builder)
                .andExpect(status().isUnauthorized());
    }

    public Object[] parametersForUserAuthorityTest() {
        return $(
                $(post("app/create/application/user/1")),
                $(post("app/update/application/user/2")),
                $(delete("app/delete/application/1"))
        );
    }
}
