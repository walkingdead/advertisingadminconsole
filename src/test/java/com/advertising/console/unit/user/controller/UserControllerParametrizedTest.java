package com.advertising.console.unit.user.controller;

import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.user.UserController;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.user.UserService;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junitparams.JUnitParamsRunner.$;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(JUnitParamsRunner.class)
@WebMvcTest(UserController.class)
@WithMockUser
public class UserControllerParametrizedTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        mvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @MockBean
    private UserService userService;

    private static final Gson gson = new Gson();

    private static final String LOGIN_REQUIRED = "login is required";
    private static final String PASSWORD_REQUIRED = "password is required";
    private static final String EMAIL_REQUIRED = "email is required";
    private static final String AUTHORITIES_REQUIRED = "authorities is required";

    private static final String LOGIN_NOT_NULL = "login can't be null";
    private static final String PASSWORD_NOT_NULL = "password can't be null";
    private static final String EMAIL_NOT_NULL = "email can't be null";
    private static final String AUTHORITIES_NOT_NULL = "authorities can't be null";

    @Test
    @Parameters(method = "parametersForUserTest")
    public void createAdoptTest(User user, ResultMatcher status, String[] errorMessages) throws Exception {
        verifyResults(user, status, errorMessages, post("/users/adopt"));
    }

    @Test
    @Parameters(method = "parametersForUserTest")
    public void updateAdoptTest(User user, ResultMatcher status, String[] errorMessages) throws Exception {
        verifyResults(user, status, errorMessages, put("/users/adopt"));
    }

    public Object[] parametersForUserTest() {
        return $(
                $(new User(EMPTY, EMPTY, EMPTY, null), status().isBadRequest(),
                        $string(LOGIN_REQUIRED, PASSWORD_REQUIRED, EMAIL_REQUIRED, AUTHORITIES_REQUIRED)),
                $(new User("demoUser", EMPTY, EMPTY, null), status().isBadRequest(),
                        $string(PASSWORD_REQUIRED, EMAIL_REQUIRED, AUTHORITIES_REQUIRED)),
                $(new User("demoUser", "querty", EMPTY, null), status().isBadRequest(),
                        $string(EMAIL_REQUIRED, AUTHORITIES_REQUIRED)),
                $(new User("demoUser", "querty", "querty@gmauil.com", null), status().isBadRequest(),
                        $string(AUTHORITIES_REQUIRED)),
                $(new User("demoUser", "querty", "querty@gmauil.com",
                                Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER))), status().is2xxSuccessful(),
                        $string()),
                $(new User(null, null, null, null), status().isBadRequest(),
                        $string(LOGIN_NOT_NULL, PASSWORD_NOT_NULL, EMAIL_NOT_NULL, AUTHORITIES_NOT_NULL)),
                $(new User("demoUser", null, null, null), status().isBadRequest(),
                        $string(PASSWORD_NOT_NULL, EMAIL_NOT_NULL, AUTHORITIES_NOT_NULL)),
                $(new User("demoUser", "querty", null, null), status().isBadRequest(),
                        $string(EMAIL_NOT_NULL, AUTHORITIES_NOT_NULL)),
                $(new User("demoUser", "querty", "querty@gmauil.com", null), status().isBadRequest(),
                        $string(AUTHORITIES_NOT_NULL))
        );
    }


    @Test
    @Parameters(method = "parametersForDeleteUserTest")
    public void deleteUserTest(MockHttpServletRequestBuilder builder, ResultMatcher resultMatcher) throws Exception {
        this.mvc.perform(builder)
                .andExpect(resultMatcher);
    }

    public Object[] parametersForDeleteUserTest() {
        return $(
                $(delete("/users/adopt/wer"), status().isInternalServerError()),
                $(delete("/users/publisher/wer"), status().isInternalServerError()),
                $(delete("/users/adopt/12"), status().isNoContent()),
                $(delete("/users/publisher/18"), status().isNoContent())
        );
    }

    @Test
    public void getUsersWithAuthorityTest() throws Exception {
        this.mvc.perform(get("/users/authority/ "))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getUsersByIdTest() throws Exception {
        this.mvc.perform(get("/users/user/ "))
                .andExpect(status().isBadRequest());
    }

    private void verifyResults(User user, ResultMatcher status, String[] errorMessages,
                               MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult result = this.mvc.perform(builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(user)))
                .andExpect(status)
                .andReturn();
        String rawResponse = result.getResponse().getContentAsString();
        List<String> errors = new ArrayList<>();
        if (StringUtils.isNotBlank(rawResponse))
            errors = Arrays.asList(rawResponse.substring(rawResponse.indexOf('[') + 1, rawResponse.lastIndexOf(']')).split(", "));
        assertThat(errors, hasItems(errorMessages));
    }

    public static String[] $string(String... params) {
        return params;
    }
}
