package com.advertising.console.common;


public final class ScriptConstants {

    private ScriptConstants(){}

    public static final String CREATE_ADMIN_SCRIPT = "classpath:/scripts/users/create-admin.sql";
    public static final String CREATE_ADOPT_SCRIPT = "classpath:/scripts/users/create-adopt.sql";
    public static final String CREATE_PUBLISHER_SCRIPT = "classpath:/scripts/users/create-publisher.sql";

    public static final String TRUNCATE_USERS_SCRIPT  = "classpath:/scripts/users/truncate-users.sql";


    public static final String CREATE_ADOPT_APPLICATION_SCRIPT = "classpath:/scripts/applications/create-adopt-app.sql";
    public static final String CREATE_PUBLISHER_APPLICATION_SCRIPT = "classpath:/scripts/applications/create-publisher-app.sql";

    public static final String TRUNCATE_APPLICATION_SCRIPT  = "classpath:/scripts/applications/truncate-apps.sql";

}