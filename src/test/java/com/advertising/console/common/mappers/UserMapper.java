package com.advertising.console.common.mappers;

import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.user.domain.meta.AuthorityDbMetaInfo;
import com.advertising.console.user.domain.meta.UserDbMetaInfo;
import com.google.common.collect.Sets;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(UserDbMetaInfo.USERS_ID));
        user.setLogin(resultSet.getString(UserDbMetaInfo.LOGIN));
        user.setPassword(resultSet.getString(UserDbMetaInfo.PASSWORD));
        user.setEmail(resultSet.getString(UserDbMetaInfo.EMAIL));

        Authority authority = new Authority();
        authority.setId(resultSet.getLong(AuthorityDbMetaInfo.AUTHORITY_ID));
        authority.setName(resultSet.getString(AuthorityDbMetaInfo.AUTHORITY_NAME));
        user.setAuthorities(Sets.newHashSet(authority));
        return user;
    }
}
