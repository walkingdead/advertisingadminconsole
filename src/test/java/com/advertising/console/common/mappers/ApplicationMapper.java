package com.advertising.console.common.mappers;

import com.advertising.console.application.Application;
import com.advertising.console.application.enums.AppType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ApplicationMapper implements RowMapper<Application> {

    @Override
    public Application mapRow(ResultSet resultSet, int i) throws SQLException {

        Application application = new Application(resultSet.getString("APPLICATION_NAME"), null,
                AppType.valueOf(resultSet.getString("APPLICATION_TYPE")));
        application.setId(resultSet.getLong("APPLICATION_ID"));

        return application;
    }
}
