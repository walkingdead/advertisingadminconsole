package com.advertising.console.common;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class SecurityAuthenticator {

    public HttpHeaders getAuthHeader(RestTemplate restTemplate, String port, String name, String password) {
        MultiValueMap<String, String> securityHeaders = new LinkedMultiValueMap<>();
        securityHeaders.add("Content-Type", "application/x-www-form-urlencoded");
        HttpEntity<String> credentials = new HttpEntity<>(String.format("username=%s&password=%s", name, password), securityHeaders);
        HttpEntity<String> authResponse = restTemplate.exchange("http://localhost:{port}/authenticate",
                HttpMethod.POST, credentials, String.class, port);
        HttpHeaders authHeaders = authResponse.getHeaders();

        HttpHeaders headers = new HttpHeaders();
        headers.put("Cookie", authHeaders.get("Set-Cookie"));

        return headers;
    }
}
