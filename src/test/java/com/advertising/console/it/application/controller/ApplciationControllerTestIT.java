package com.advertising.console.it.application.controller;


import com.advertising.console.common.SecurityAuthenticator;
import com.advertising.console.common.annotation.FunctionalTestRunner;
import com.advertising.console.common.mappers.ApplicationMapper;
import com.advertising.console.common.mappers.UserMapper;
import com.advertising.console.application.Application;
import com.advertising.console.user.domain.User;
import com.advertising.console.application.enums.AppType;
import com.advertising.console.application.enums.ContentType;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;

import static com.advertising.console.common.ScriptConstants.CREATE_ADOPT_APPLICATION_SCRIPT;
import static com.advertising.console.common.ScriptConstants.CREATE_ADOPT_SCRIPT;
import static com.advertising.console.common.ScriptConstants.CREATE_PUBLISHER_APPLICATION_SCRIPT;
import static com.advertising.console.common.ScriptConstants.CREATE_PUBLISHER_SCRIPT;
import static com.advertising.console.common.ScriptConstants.TRUNCATE_APPLICATION_SCRIPT;
import static com.advertising.console.common.ScriptConstants.TRUNCATE_USERS_SCRIPT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class ApplciationControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SecurityAuthenticator securityAuthenticator;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${local.server.port}")
    private String port;

    private static final String ADOPT_LOGIN = "adopt@gmail.com";
    private static final String ADOPT_PASSWORD = "admin";

    private static final String PUBLISHER_LOGIN = "publisher@gmail.com";
    private static final String PUBLISHER_PASSWORD = "admin";

    private static final Gson gson = new Gson();

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT})
    public void testCreateApplication() {

        User adopt = findUserByEmail(ADOPT_LOGIN);

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(createDummyApplication("adoprtDemo")),
                headers);

        ResponseEntity<Application> response = restTemplate.exchange(
                "http://localhost:{port}/app/application/user/{userId}",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<Application>() {
                }, port, adopt.getId());

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        Application application = response.getBody();

        assertThat("Application was created", application, notNullValue());
        assertThat("Application id is present", application.getId(), notNullValue());
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_PUBLISHER_SCRIPT, TRUNCATE_APPLICATION_SCRIPT, CREATE_PUBLISHER_APPLICATION_SCRIPT})
    public void testUpdateApplication() {

        User savedPublisher = findUserByEmail(PUBLISHER_LOGIN);
        assertThat("Publisher Exists", savedPublisher, notNullValue());

        Application publisherApp = findApplicationByName("publisherApp");
        assertThat("Publisher application Exists", publisherApp, notNullValue());

        publisherApp.setAppType(AppType.IOS);
        publisherApp.setContentTypes(Sets.newHashSet(ContentType.IMAGE, ContentType.HTML));

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, PUBLISHER_LOGIN, PUBLISHER_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(publisherApp),
                headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/app/application/user/{userId}",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<String>() {
                }, port, savedPublisher.getId());

        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        Application updateAdoptApp = findApplicationByName("publisherApp");

        assertThat("Publisher app type was changed", updateAdoptApp.getAppType(), is(AppType.IOS));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT, TRUNCATE_APPLICATION_SCRIPT, CREATE_ADOPT_APPLICATION_SCRIPT})
    public void testDeleteAdopt() {

        Application publisherApp = findApplicationByName("adoptApp");
        assertThat("Adopt application Exists", publisherApp, notNullValue());

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/app/application/{appId}",
                HttpMethod.DELETE, authEntity, new ParameterizedTypeReference<String>() {
                }, port, publisherApp.getId());
        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        boolean adoptAppExists = isApplicationExists(publisherApp.getId());
        assertThat("Adopt application should be deleted", adoptAppExists, is(false));
    }

    private static Application createDummyApplication(String appName) {
        HashSet<ContentType> contentTypes = Sets.newHashSet(ContentType.IMAGE, ContentType.HTML);
        return new Application("Dummy", contentTypes, AppType.ANDROID);
    }

    private User findUserByEmail(String email) {
        return jdbcTemplate.queryForObject("SELECT * FROM USERS as u INNER JOIN USERS_AUTHORITY as ua " +
                "ON u.USERS_ID = ua.USER_ID INNER JOIN AUTHORITY as a ON ua.AUTHORITY_ID = a.AUTHORITY_ID " +
                "where u.email = ?", new UserMapper(), email);
    }

    private Application findApplicationByName(String appName) {
        return jdbcTemplate.queryForObject("SELECT * FROM application WHERE application_name = ? ",
                new ApplicationMapper(), appName);

    }

    private boolean isApplicationExists(Long appId) {
        return jdbcTemplate.query("SELECT * FROM application WHERE application_id = ?",
                new Object[]{appId}, new BeanPropertyRowMapper<>(Application.class)).size() != 0;
    }
}
