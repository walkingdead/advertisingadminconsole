package com.advertising.console.it.application.service;

import com.advertising.console.common.annotation.IntegrationTestRunner;
import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.common.mappers.ApplicationMapper;
import com.advertising.console.application.Application;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.application.enums.AppType;
import com.advertising.console.application.enums.ContentType;
import com.advertising.console.application.ApplicationRepository;
import com.advertising.console.user.persistence.AuthorityRepository;
import com.advertising.console.user.persistence.UserRepository;
import com.advertising.console.application.ApplicationService;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ApplicationServiceTestIT {

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private User adopt;
    private User publisher;

    @Before
    public void init(){
        Map<String, Authority> authorities = getAuthorities();
        adopt = userRepository.save(createDummyUser(UserAuthorities.ADOPT));
        adopt.setAuthorities(getPersistedAuthorities(adopt.getAuthorities(), authorities));
        publisher = userRepository.save(createDummyUser(UserAuthorities.PUBLISHER));
        publisher.setAuthorities(getPersistedAuthorities(publisher.getAuthorities(), authorities));
        userRepository.flush();
    }

    @Test
    @WithMockUser(authorities = {"ADOPT", "PUBLISHER"})
    public void createApplicationTest() {

        Application adoptDemo = applicationService.saveApplication(createDummyApplication("adoptDemo"),
                adopt.getId());
        assertThat("Adopt application was created", adoptDemo, notNullValue());
        assertThat("Adopt application id not null", adoptDemo.getId(), notNullValue());

        Application publisherDemo = applicationService.saveApplication(createDummyApplication("PublisherDemo"),
                publisher.getId());
        assertThat("Publisher application was created", publisherDemo, notNullValue());
        assertThat("Publisher application id not null", publisherDemo.getId(), notNullValue());
    }

    @Test
    @WithMockUser(authorities = {"ADOPT", "PUBLISHER"})
    public void updateApplicationTest() {

        Application adoptApp = applicationService.saveApplication(createDummyApplication("adoptApp"),
                adopt.getId());

        adoptApp.setName("Real adopt project");
        applicationService.updateApplication(adoptApp, adopt.getId());
        applicationRepository.flush();
        Application updatedAdoptApp = findApplicationById(adoptApp.getId());
        assertThat("Adopt application name was changed", updatedAdoptApp.getName(), is("Real adopt project"));

        Application publisherApp = applicationService.saveApplication(createDummyApplication("PublisherApp"),
                publisher.getId());

        publisherApp.setAppType(AppType.IOS);
        applicationService.updateApplication(publisherApp, publisher.getId());
        applicationRepository.flush();
        Application updatedPublisherApp = findApplicationById(publisherApp.getId());

        assertThat("Publisher application appType was changed", updatedPublisherApp.getAppType(), is(AppType.IOS));
    }

    @Test
    @WithMockUser(authorities = {"ADOPT", "PUBLISHER"})
    public void deleteApplicationTest() {

        Application adoptApp = applicationService.saveApplication(createDummyApplication("adoptApp"),
                adopt.getId());
        applicationRepository.flush();
        applicationService.deleteApplication(adoptApp.getId());
        applicationRepository.flush();
        boolean updatedAdoptAppExist = isApplicationExists(adoptApp.getId());

        assertThat("Adopt application should be removed", updatedAdoptAppExist, is(false));

        Application publisherApp = applicationService.saveApplication(createDummyApplication("PublisherApp"),
                publisher.getId());
        applicationRepository.flush();
        applicationService.deleteApplication(publisherApp.getId());
        applicationRepository.flush();
        boolean updatedPublisherAppExists = isApplicationExists(publisherApp.getId());

        assertThat("Publisher application appType was changed", updatedPublisherAppExists, is(false));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void adminCreateApplicationTest(){
        expectedException.expect(AccessDeniedException.class);
        applicationService.saveApplication(new Application(), 1L);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void adminDeleteApplicationTest(){
        expectedException.expect(AccessDeniedException.class);
        applicationService.deleteApplication(1L);
    }

    private Map<String, Authority> getAuthorities() {
        return authorityRepository.findAll().stream()
                .collect(Collectors.toMap(Authority::getName, Function.identity()));
    }

    private static Set<Authority> getPersistedAuthorities(Set<Authority> userAuthorities, Map<String, Authority> authorities){
        return userAuthorities.stream().map(authority -> authorities.get(authority.getName())).collect(Collectors.toSet());
    }

    private Application findApplicationById(Long applicationId) {
        return jdbcTemplate.queryForObject("SELECT * FROM application WHERE application_id = ? ",
                new ApplicationMapper(), applicationId );

    }

    private boolean isApplicationExists(Long applicationId) {
        return jdbcTemplate.query("SELECT * FROM application WHERE application_id = ?",
                new Object[]{applicationId}, new BeanPropertyRowMapper<>(Application.class)).size() != 0;
    }

    private User createDummyUser(String authority) {
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(authority));
        return new User("Dummy", "qwerty", authority + "@gmail.com", authorities);
    }

    private Application createDummyApplication(String appName) {
        HashSet<ContentType> contentTypes = Sets.newHashSet(ContentType.HTML, ContentType.VIDEO);
        return new Application(appName, contentTypes, AppType.ANDROID);
    }

}
