package com.advertising.console.it.user.controller;


import com.advertising.console.common.SecurityAuthenticator;
import com.advertising.console.common.annotation.FunctionalTestRunner;
import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.common.mappers.UserMapper;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;

import static com.advertising.console.common.ScriptConstants.CREATE_ADMIN_SCRIPT;
import static com.advertising.console.common.ScriptConstants.CREATE_ADOPT_SCRIPT;
import static com.advertising.console.common.ScriptConstants.CREATE_PUBLISHER_SCRIPT;
import static com.advertising.console.common.ScriptConstants.TRUNCATE_USERS_SCRIPT;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class UserControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SecurityAuthenticator securityAuthenticator;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${local.server.port}")
    private String port;

    private static final String ADMIN_LOGIN = "admin@gmail.com";
    private static final String ADMIN_PASSWORD = "admin";

    private static final String ADOPT_LOGIN = "adopt@gmail.com";
    private static final String ADOPT_PASSWORD = "admin";

    private static final String PUBLISHER_LOGIN = "publisher@gmail.com";
    private static final String PUBLISHER_PASSWORD = "admin";

    private static final Gson gson = new Gson();

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADMIN_SCRIPT})
    public void testCreateAdopt() {

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(createDummyUser(UserAuthorities.ADOPT)),
                headers);

        ResponseEntity<User> response = restTemplate.exchange(
                "http://localhost:{port}/users/adopt",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<User>() {
                }, port);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        User adopt = response.getBody();

        assertThat("Adopt was created", adopt, notNullValue());
        assertThat("Adopt id is present", adopt.getId(), notNullValue());
        assertThat("Adopt has ADOPT AUTHORITY", adopt.getAuthorities()
                .stream().map(Authority::getName).collect(toList()), hasItem(UserAuthorities.ADOPT));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT})
    public void testCreatePublisher() {

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(createDummyUser(UserAuthorities.PUBLISHER)),
                headers);

        ResponseEntity<User> response = restTemplate.exchange(
                "http://localhost:{port}/users/publisher",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<User>() {
                }, port);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        User adopt = response.getBody();

        assertThat("Publisher was created", adopt, notNullValue());
        assertThat("Publisher id is present", adopt.getId(), notNullValue());
        assertThat("Publisher has PUBLISHER AUTHORITY", adopt.getAuthorities()
                .stream().map(Authority::getName).collect(toList()), hasItem(UserAuthorities.PUBLISHER));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADMIN_SCRIPT, CREATE_ADOPT_SCRIPT})
    public void testUpdateAdopt() {

        User savedAdopt = findUserByEmail(ADOPT_LOGIN);
        assertThat("Adopt Exists", savedAdopt, notNullValue());

        savedAdopt.setLogin("assassin");

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(savedAdopt),
                headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/users/adopt",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<String>() {
                }, port);

        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        User updateAdopt = findUserByEmail(ADOPT_LOGIN);
        assertThat("Adopt login was changed", updateAdopt.getLogin(), is("assassin"));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT, CREATE_PUBLISHER_SCRIPT})
    public void testUpdatePublisher() {

        User savedPublisher = findUserByEmail(PUBLISHER_LOGIN);
        assertThat("Publisher Exists", savedPublisher, notNullValue());

        savedPublisher.setLogin("DeleteMe");

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> authEntity = new HttpEntity<>(gson.toJson(savedPublisher),
                headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/users/publisher",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<String>() {
                }, port);
        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        User updateAdopt = findUserByEmail(PUBLISHER_LOGIN);
        assertThat("Publisher login was changed", updateAdopt.getLogin(), is("DeleteMe"));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADMIN_SCRIPT, CREATE_ADOPT_SCRIPT})
    public void testDeleteAdopt() {

        User savedAdopt = findUserByEmail(ADOPT_LOGIN);
        assertThat("Adopt Exists", savedAdopt, notNullValue());

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/users/adopt/{adoptId}",
                HttpMethod.DELETE, authEntity, new ParameterizedTypeReference<String>() {
                }, port, savedAdopt.getId());
        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        boolean adoptExists = isUserExists(ADOPT_LOGIN);
        assertThat("Adopt should be deleted", adoptExists, is(false));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT, CREATE_PUBLISHER_SCRIPT})
    public void testDeletePublisher() {

        User savedPublisher = findUserByEmail(PUBLISHER_LOGIN);
        assertThat("Publisher Exists", savedPublisher, notNullValue());

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:{port}/users/publisher/{publisherId}",
                HttpMethod.DELETE, authEntity, new ParameterizedTypeReference<String>() {
                }, port, savedPublisher.getId());
        assertThat("Response status should be no content", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        boolean adoptExists = isUserExists(PUBLISHER_LOGIN);
        assertThat("Adopt should be deleted", adoptExists, is(false));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADMIN_SCRIPT, CREATE_ADOPT_SCRIPT, CREATE_PUBLISHER_SCRIPT})
    public void testGetNonAdminUsers() {

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<List<User>> response = restTemplate.exchange(
                "http://localhost:{port}/users/nonadmins",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<List<User>>() {
                }, port);
        assertThat("Response status should be no ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<User> users = response.getBody();
        assertThat("Two users with non admin rights found", users, hasSize(2));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT, CREATE_ADOPT_SCRIPT, CREATE_PUBLISHER_SCRIPT})
    public void testGetUsersWithRole() {

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADOPT_LOGIN, ADOPT_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<List<User>> response = restTemplate.exchange(
                "http://localhost:{port}/users/authority/PUBLISHER",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<List<User>>() {
                }, port);
        assertThat("Response status should be no ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<User> users = response.getBody();
        assertThat("One users with authority PUBLISHER found", users, hasSize(1));
    }

    @Test
    @Sql(scripts = {TRUNCATE_USERS_SCRIPT,  CREATE_ADMIN_SCRIPT, CREATE_ADOPT_SCRIPT, CREATE_PUBLISHER_SCRIPT})
    public void testGetUserById() {

        User savedPublisher = findUserByEmail(PUBLISHER_LOGIN);

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<User> response = restTemplate.exchange(
                "http://localhost:{port}/users/user/{userId}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<User>() {
                }, port, savedPublisher.getId());
        assertThat("Response status should be no ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        User user = response.getBody();
        assertThat("One users with authority PUBLISHER found", user.getId(), is(savedPublisher.getId()));
    }

    private User findUserByEmail(String email) {
        return jdbcTemplate.queryForObject("SELECT * FROM USERS as u INNER JOIN USERS_AUTHORITY as ua " +
                        "ON u.USERS_ID = ua.USER_ID INNER JOIN AUTHORITY as a ON ua.AUTHORITY_ID = a.AUTHORITY_ID " +
                        "where u.email = ?", new UserMapper(), email);
    }

    private User createDummyUser(String authority) {
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(authority));
        return new User("Dummy", "qwerty", authority + "@gmail.com", authorities);
    }

    private boolean isUserExists(String email) {
        return jdbcTemplate.query("SELECT * FROM users WHERE email = ?",
                new Object[]{email}, new BeanPropertyRowMapper<>(User.class)).size() != 0;
    }
}
