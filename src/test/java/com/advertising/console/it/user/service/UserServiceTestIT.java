package com.advertising.console.it.user.service;

import com.advertising.console.common.annotation.IntegrationTestRunner;
import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.user.persistence.UserRepository;
import com.advertising.console.user.UserService;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserServiceTestIT {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void createAdoptByAdminTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.ADOPT));
        User user = userService.createAdopt(new User("John", "12345", "john@gmail.com", authorities));

        assertThat("Adopt was created by Admin", user, notNullValue());
        assertThat("Adopt id not null", user.getId(), notNullValue());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void createPublisherByAdminTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER));
        User user = userService.createPublisher(new User("Bill", "98765", "bill@gmail.com", authorities));

        assertThat("Publisher was created by Admin", user, notNullValue());
        assertThat("Publisher id not null", user.getId(), notNullValue());
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void createPublisherByAdoptTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER));
        User publisher = userService.createPublisher(new User("Lenny", "qwerty", "lenny@gmail.com", authorities));

        assertThat("Publisher was created by Adopt", publisher, notNullValue());
        assertThat("Publisher id not null", publisher.getId(), notNullValue());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void updateAdoptByAdminTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.ADOPT));
        User adopt = userService.createAdopt(new User("John", "12345", "john@gmail.com", authorities));
        adopt.setEmail("john@yahoo.com");

        userService.updateAdopt(adopt);
        userRepository.flush();

        User updatedAdopt = findUserById(adopt.getId());

        assertThat("Adopt email should be changed", updatedAdopt.getEmail(), is("john@yahoo.com"));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void updatePublisherByAdminTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER));
        User publisher = userService.createAdopt(new User("Erik", "12345", "erik@gmail.com", authorities));
        publisher.setLogin("Charles");
        userService.updatePublisher(publisher);
        userRepository.flush();

        User updatedPublisher= findUserById(publisher.getId());

        assertThat("Publisher login should be changed", updatedPublisher.getLogin(), is("Charles"));
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void updatePublisherByAdoptTest(){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(UserAuthorities.PUBLISHER));
        User publisher = userService.createPublisher(new User("Frank", "1354156", "frank@gmail.com", authorities));
        publisher.setLogin("Leon");
        publisher.setEmail("frank@i.ua");
        userService.updatePublisher(publisher);
        userRepository.flush();

        User updatedPublisher = findUserById(publisher.getId());

        assertThat("Publisher login should be changed", updatedPublisher.getLogin(), is("Leon"));
        assertThat("Publisher email should be changed", updatedPublisher.getEmail(), is("frank@i.ua"));
    }


    @Test
    @WithMockUser(authorities = "ADMIN")
    public void deletePublisherByAdminTest(){
        User publisher = userService.createPublisher(createDummyUser(UserAuthorities.PUBLISHER));;
        userService.deletePublisher(publisher.getId());
        userRepository.flush();

        boolean userExists = isUserExists(publisher.getId());

        assertThat("Publisher should be deleted", userExists, is(false));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void deleteAdoptByAdminTest(){
        User adopt = userService.createPublisher(createDummyUser(UserAuthorities.ADOPT));;
        userService.deleteAdopt(adopt.getId());
        userRepository.flush();

        boolean userExists = isUserExists(adopt.getId());

        assertThat("Adopt should be deleted", userExists, is(false));
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void deletePublisherByAdoptTest(){
        User publisher = userService.createPublisher(createDummyUser(UserAuthorities.PUBLISHER));;
        userService.deletePublisher(publisher.getId());
        userRepository.flush();

        boolean userExists = isUserExists(publisher.getId());

        assertThat("Adopt should be deleted", userExists, is(false));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void getNonAdminUsersTest(){
        userRepository.deleteAll();
        userService.createPublisher(createDummyUser(UserAuthorities.ADMIN));
        userService.createAdopt(createDummyUser(UserAuthorities.ADOPT));
        userRepository.flush();

        List<User> nonAdminUsers = userService.getNonAdminUsers();

        assertThat("Only one user should be found", nonAdminUsers, hasSize(1));
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void getUsersWithAuthorityTest(){
        userService.createPublisher(createDummyUser(UserAuthorities.PUBLISHER));
        userRepository.flush();

        List<User> nonAdminUsers = userService.getUsersWithAuthority(UserAuthorities.PUBLISHER);

        assertThat("Only one user should be found", nonAdminUsers, hasSize(1));
    }

    @Test
    @WithMockUser(authorities = "PUBLISHER")
    public void publisherCreatesAdoptTest(){
        expectedException.expect(AccessDeniedException.class);
        userService.createAdopt(createDummyUser(UserAuthorities.ADOPT));
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void adoptUpdateAdoptTest(){
        expectedException.expect(AccessDeniedException.class);
        userService.updateAdopt(createDummyUser(UserAuthorities.ADOPT));
    }

    @Test
    @WithMockUser(authorities = "PUBLISHER")
    public void publisherDeletesAdoptTest(){
        expectedException.expect(AccessDeniedException.class);
        userService.deleteAdopt(1L);
    }

    @Test
    @WithMockUser(authorities = "ADOPT")
    public void adoptGetNonAdminUsersTest(){
        expectedException.expect(AccessDeniedException.class);
        userService.getNonAdminUsers();
    }

    private User findUserById(Long userId){
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE users_id = ? ",
                new BeanPropertyRowMapper<>(User.class), userId);

    }

    private boolean isUserExists (Long userId) {
        return jdbcTemplate.query("SELECT * FROM users WHERE users_id = ?",
                new Object[]{userId}, new BeanPropertyRowMapper<>(User.class)).size() != 0;
    }

    private User createDummyUser(String authority){
        HashSet<Authority> authorities = Sets.newHashSet(new Authority(authority));
       return  new User("Dummy", "qwerty", authority + "@gmail.com", authorities);
    }
}
