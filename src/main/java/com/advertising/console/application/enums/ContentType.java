package com.advertising.console.application.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ContentType {

    VIDEO("VIDEO"),
    IMAGE("IMAGE"),
    HTML("HTML");

    private final String name;

    ContentType(String name) {
        this.name = name();
    }

    @JsonCreator
    public static ContentType forValue(String value) {
        return ContentType.valueOf(value);
    }
}

