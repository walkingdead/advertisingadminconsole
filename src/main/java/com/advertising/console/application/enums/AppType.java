package com.advertising.console.application.enums;


import com.fasterxml.jackson.annotation.JsonCreator;

public enum AppType {

    IOS("IOS"),
    ANDROID("ANDROID"),
    WEBSITE("WEBSITE");

    private final String name;

    AppType(String name) {
        this.name = name();
    }

    @JsonCreator
    public static AppType forValue(String value) {
        return AppType.valueOf(value);
    }
}
