package com.advertising.console.application;

import com.advertising.console.application.enums.AppType;
import com.advertising.console.application.enums.ContentType;
import com.advertising.console.user.domain.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "APPLICATION")
public class Application {

    @Id
    @GeneratedValue
    @Column(name = "APPLICATION_ID")
    private Long id;

    @Column(name = "APPLICATION_NAME")
    @NotNull(message = "name can't be null")
    @NotBlank(message = "name is required")
    private String name;

    @ElementCollection(targetClass = ContentType.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "CONTENT_TYPES", joinColumns = @JoinColumn(name = "CONTENT_TYPE_ID"))
    @Column(name = "CONTENT_TYPE")
    @Enumerated(EnumType.STRING)
    @NotEmpty(message = "contentTypes are required")
    @Fetch(FetchMode.SELECT)
    private Set<ContentType> contentTypes = new HashSet<>();

    @Column(name = "APPLICATION_TYPE")
    @Enumerated(EnumType.STRING)
    @NotNull (message = "appType can't be null")
    private AppType appType;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    @JsonBackReference
    private User user;

    public Application() {
    }

    public Application(String name, Set<ContentType> contentTypes, AppType appType) {
        this.name = name;
        this.contentTypes = contentTypes;
        this.appType = appType;
    }

    public Application(String name, Set<ContentType> contentTypes, AppType appType, User user) {
        this.name = name;
        this.contentTypes = contentTypes;
        this.appType = appType;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ContentType> getContentTypes() {
        return contentTypes;
    }

    public void setContentTypes(Set<ContentType> contentTypes) {
        this.contentTypes = contentTypes;
    }

    public AppType getAppType() {
        return appType;
    }

    public void setAppType(AppType appType) {
        this.appType = appType;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("contentTypes", contentTypes)
                .append("appType", appType)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(23, 79)
                .append(id)
                .append(name)
                .append(appType)
                .append(contentTypes)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != Application.class) {
            return false;
        }
        Application other = (Application) obj;

        return new EqualsBuilder()
                .append(this.id, other.id)
                .append(this.name, other.name)
                .append(this.appType, other.appType)
                .append(this.contentTypes, other.contentTypes)
                .isEquals();
    }
}
