package com.advertising.console.application;

import com.advertising.console.application.Application;
import com.advertising.console.application.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/app")
@Validated
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.POST)
    public ResponseEntity createApplication(@RequestBody @Valid Application application, @PathVariable("userId") Long userId){
        return ResponseEntity.ok(applicationService.saveApplication(application, userId));
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.PUT)
    public ResponseEntity updateApplication(@RequestBody @Valid Application application, @PathVariable("userId") Long userId){
        applicationService.updateApplication(application, userId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{applicationId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteApplication(@PathVariable("applicationId") @NotNull Long applicationId){
        applicationService.deleteApplication(applicationId);
        return ResponseEntity.noContent().build();
    }
}
