package com.advertising.console.application;

import com.advertising.console.user.domain.User;
import com.advertising.console.user.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private UserRepository userRepository;

    @PreAuthorize("hasAnyAuthority('ADOPT, PUBLISHER')")
    public Application saveApplication(Application application, Long userId) {
        User user = userRepository.findOne(userId);
        application.setUser(user);
        return applicationRepository.save(application);
    }

    @PreAuthorize("hasAnyAuthority('ADOPT, PUBLISHER')")
    public void deleteApplication(Long applicationId) {
        applicationRepository.delete(applicationId);
    }

    @PreAuthorize("hasAnyAuthority('ADOPT, PUBLISHER')")
    public void updateApplication(Application application, Long userId) {
        User user = userRepository.findOne(userId);
        application.setUser(user);
        applicationRepository.save(application);
    }
}
