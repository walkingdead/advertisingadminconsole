package com.advertising.console.user.domain.meta;


public final class UserDbMetaInfo {

    private UserDbMetaInfo(){}

    public static final String USERS_ID = "USERS_ID";
    public static final String LOGIN = "LOGIN";
    public static final String PASSWORD = "PASSWORD";
    public static final String EMAIL = "EMAIL";
}
