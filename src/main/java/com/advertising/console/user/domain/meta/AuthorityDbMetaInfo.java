package com.advertising.console.user.domain.meta;


public final class AuthorityDbMetaInfo {

    private AuthorityDbMetaInfo(){}

    public static final String AUTHORITY_ID = "AUTHORITY_ID";
    public static final String AUTHORITY_NAME = "AUTHORITY_NAME";
    
}
