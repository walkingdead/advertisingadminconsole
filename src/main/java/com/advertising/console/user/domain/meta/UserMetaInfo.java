package com.advertising.console.user.domain.meta;


public final class UserMetaInfo {

    private UserMetaInfo(){}

    public static final String AUTHORITIES = "authorities";
}
