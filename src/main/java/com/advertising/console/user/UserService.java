package com.advertising.console.user;

import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.user.persistence.AuthorityRepository;
import com.advertising.console.user.persistence.UserRepository;
import com.advertising.console.user.persistence.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder encoder;

    @PreAuthorize("hasAuthority('ADMIN')")
    public User createAdopt(User adopt){
        Map<String, Authority> authorities = authorityRepository.findAll().stream()
                .collect(Collectors.toMap(Authority::getName, Function.identity()));
        adopt.setPassword(encoder.encode(adopt.getPassword()));
        adopt.setAuthorities(getPersistedAuthorities(adopt.getAuthorities(), authorities));
        return userRepository.save(adopt);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN, ADOPT')")
    public User createPublisher(User publisher){
        Map<String, Authority> authorities = authorityRepository.findAll().stream()
                .collect(Collectors.toMap(Authority::getName, Function.identity()));
        publisher.setPassword(encoder.encode(publisher.getPassword()));
        publisher.setAuthorities(getPersistedAuthorities(publisher.getAuthorities(), authorities));
        return userRepository.save(publisher);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public void updateAdopt(User adopt){
        User adoptUser = userRepository.findOne(adopt.getId());
        Map<String, Authority> authorities = authorityRepository.findAll().stream()
                .collect(Collectors.toMap(Authority::getName, Function.identity()));
        adopt.setPassword(adoptUser.getPassword());
        adopt.setAuthorities(getPersistedAuthorities(adopt.getAuthorities(), authorities));
        userRepository.save(adopt);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN, ADOPT')")
    public void updatePublisher(User publisher){
        User publisherUser = userRepository.findOne(publisher.getId());
        Map<String, Authority> authorities = authorityRepository.findAll().stream()
                .collect(Collectors.toMap(Authority::getName, Function.identity()));
        publisher.setPassword(publisherUser.getPassword());
        publisher.setAuthorities(getPersistedAuthorities(publisher.getAuthorities(), authorities));
        userRepository.save(publisher);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteAdopt(Long adoptId){
        userRepository.delete(adoptId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN, ADOPT')")
    public void deletePublisher(Long publisherId){
        userRepository.delete(publisherId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public List<User> getNonAdminUsers(){
        return userRepository.findAll(UserSpecification.getNonAdminUsers());
    }

    @PreAuthorize("hasAuthority('ADOPT')")
    public List<User> getUsersWithAuthority(String authority){
        return userRepository.findAll(UserSpecification.getUsersWithAuthority(authority));
    }

    public User getUserById(Long userId){
        return userRepository.findOne(userId);
    }

    private static Set<Authority> getPersistedAuthorities(Set<Authority> userAuthorities, Map<String, Authority> authorities){
        return userAuthorities.stream().map(authority -> authorities.get(authority.getName())).collect(Collectors.toSet());
    }
}
