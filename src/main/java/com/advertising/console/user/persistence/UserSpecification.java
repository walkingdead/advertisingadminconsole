package com.advertising.console.user.persistence;

import com.advertising.console.common.constants.UserAuthorities;
import com.advertising.console.user.domain.Authority;
import com.advertising.console.user.domain.User;
import com.advertising.console.user.domain.meta.AuthorityMetaInfo;
import com.advertising.console.user.domain.meta.UserMetaInfo;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;


public class UserSpecification {

    private UserSpecification(){}

    public static Specification<User> getNonAdminUsers() {
        return (root, query, cb) -> {
            Join<User, Authority> join = root.join(UserMetaInfo.AUTHORITIES);
            return cb.notEqual(join.get(AuthorityMetaInfo.NAME),UserAuthorities.ADMIN);
        };
    }

    public static Specification<User> getUsersWithAuthority(final String authority) {
        return (root, query, cb) -> {
            Join<User, Authority> join = root.join(UserMetaInfo.AUTHORITIES);
            return cb.equal(join.get(AuthorityMetaInfo.NAME), authority);
        };
    }
}

