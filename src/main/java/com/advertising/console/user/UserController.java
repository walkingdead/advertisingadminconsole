package com.advertising.console.user;

import com.advertising.console.user.domain.User;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/adopt", method = RequestMethod.POST)
    public ResponseEntity createAdopt(@RequestBody @Valid User adopt){
        return ResponseEntity.ok(userService.createAdopt(adopt));
    }

    @RequestMapping(value = "/publisher", method = RequestMethod.POST)
    public ResponseEntity createPublisher(@RequestBody @Valid  User publisher){
        return ResponseEntity.ok(userService.createPublisher(publisher));
    }

    @RequestMapping(value = "/adopt", method = RequestMethod.PUT)
    public ResponseEntity updateAdopt(@RequestBody @Valid User adopt){
        userService.updateAdopt(adopt);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/publisher", method = RequestMethod.PUT)
    public ResponseEntity updatePublisher(@RequestBody @Valid User publisher){
        userService.updatePublisher(publisher);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/adopt/{adoptId}", method = RequestMethod.DELETE)
    public ResponseEntity updateAdopt(@PathVariable("adoptId") @NotNull Long adoptId){
        userService.deleteAdopt(adoptId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/publisher/{publisherId}", method = RequestMethod.DELETE)
    public ResponseEntity updatePublisher(@PathVariable("publisherId") @NotNull Long publisherId){
        userService.deletePublisher(publisherId);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/nonadmins", method = RequestMethod.GET)
    public ResponseEntity getNonAdminUsers(){
        return ResponseEntity.ok(userService.getNonAdminUsers());
    }

    @RequestMapping(value = "/authority/{authority}", method = RequestMethod.GET)
    public ResponseEntity getUsersWithAuthority(@PathVariable("authority") @NotBlank String authority){
        return ResponseEntity.ok(userService.getUsersWithAuthority(authority));
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity getUserById(@PathVariable("userId")  @NotNull Long userId){
        return ResponseEntity.ok(userService.getUserById(userId));
    }
}
