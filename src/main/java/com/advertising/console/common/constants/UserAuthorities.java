package com.advertising.console.common.constants;


public final class UserAuthorities {

    private UserAuthorities(){}

    public static final String ADMIN = "ADMIN";
    public static final String ADOPT = "ADOPT";
    public static final String PUBLISHER = "PUBLISHER";

}
