package com.advertising.console.common;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
@RestController
public class WebConsoleControllerAdvice {

    private static final Logger LOGGER = LogManager.getLogger();

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({DataAccessException.class})
    public ErrorEntity handleSqlException(Exception exception) {
        LOGGER.error("DataBase error occurred", exception);
        return new ErrorEntity(ErrorMessage.DATA_BASE_ERROR);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity handleResourceNotFoundException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        violations.forEach(violation -> builder.append(violation.getMessage()).append("\n"));
        LOGGER.error("Constraint violations ", e.getMessage());
        return new ErrorEntity(builder.substring(0, builder.length() - 1));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity missedInputParameters(MissingServletRequestParameterException exception) {
        String errorMessage = String.format("Required [%s] parameter is missing", exception.getParameterName());
        LOGGER.error("Missing parameters ",errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity invalidInputParameters(MethodArgumentNotValidException exception) {
        String errorMessage = String.format("Parameter %s %s", exception.getParameter().getParameterName(),
                exception.getBindingResult().getAllErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(toList()));
        LOGGER.error("Not valid arguments ",errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity invalidPayload(HttpMessageNotReadableException exception) {
        LOGGER.error("Incorrect payload was provided ",exception.getMessage());
        return new ErrorEntity("Incorrect payload was provided");
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleResourceNotFoundException(Throwable exception) {
        LOGGER.error("Unknown server error occurred", exception);
        return new ErrorEntity(ErrorMessage.UNKNOWN_SERVER_ERROR);
    }
}