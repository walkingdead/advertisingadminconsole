package com.advertising.console.security.controller;



import com.advertising.console.user.domain.User;
import com.advertising.console.user.persistence.UserRepository;
import com.advertising.console.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(value = "/security/account", method = RequestMethod.GET)
    public User getUserAccount() {
        return userRepo.findByEmail(SecurityUtils.getCurrentLogin());
    }

}
