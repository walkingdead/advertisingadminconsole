var app = angular.module('advertisingAdminConsole', ['angularUtils.directives.dirPagination', 'ui.bootstrap', 'LocalStorageModule',
    'ui.router', 'http-auth-interceptor', 'ui.router.tabs', 'angularjs-dropdown-multiselect']);

app.constant('USER_ROLES', {
    all: '*',
    admin: 'ADMIN',
    adopt: 'ADOPT',
    publisher: 'PUBLISHER'
});
app.constant('APP_TYPES',
    ['ANDROID', 'IOS', 'WEBSITE']
);

app.constant('CONTENT_TYPES', [
        {label: 'video', id: 'VIDEO'},
        {label: 'image', id: 'IMAGE'},
        {label: 'html', id: 'HTML'}
    ]
);

app.constant('CONFIG', {
    baseUrl: 'http://localhost:8080/'
});

app.config(function ($stateProvider, $httpProvider, $urlRouterProvider, USER_ROLES) {
    $stateProvider
        .state('admin', {
            url: '/admin',
            templateUrl: 'html/users.html',
            controller: 'commonUserController',
            authorities : [USER_ROLES.publisher, USER_ROLES.adopt],
            resolve:{
                users : ['adminService', function (adminService) {
                    return adminService.getNonAdminUsers();
                }]
            },
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.admin]
            }
        })
        .state('adopt', {
            url: '/adopt',
            templateUrl: 'html/adopt.html',
            controller: 'adoptMainController',
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.adopt]
            }
        })
        .state('adopt.publishers', {
            url: '/publishers',
            templateUrl: 'html/users.html',
            controller: 'commonUserController',
            authorities : [USER_ROLES.publisher],
            resolve:{
                users : ['adoptService', function (adoptService) {
                    return   adoptService.getUsersWithAuthority(USER_ROLES.publisher);
                }]
            },
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.adopt]
            }
        }).state('adopt.applications', {
        url: '/applications',
        templateUrl: 'html/applications.html',
        controller: 'applicationController',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.adopt]
        }
    })
        .state('publisher', {
            url: '/publisher',
            templateUrl: 'html/applications.html',
            controller: 'applicationController',
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.publisher]
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'html/login.html',
            controller: 'loginController',
            access: {
                loginRequired: false,
                authorizedRoles: [USER_ROLES.all]
            }
        }).state('logout', {
        url: '/logout',
        templateUrl: ' ',
        controller: 'logoutController',
        access: {
            loginRequired: false,
            authorizedRoles: [USER_ROLES.all]
        }
    });
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.withCredentials = true;
});

app.run(function ($rootScope, $location, $http, authSharedService, sessionService, USER_ROLES, $q, $timeout) {

    $rootScope.$on('$routeChangeStart', function (event, next) {
        if (next.originalPath === "/login" && $rootScope.authenticated) {
            event.preventDefault();
        } else if (next.access && next.access.loginRequired && !$rootScope.authenticated) {
            event.preventDefault();
            $rootScope.$broadcast("event:auth-loginRequired", {});
        } else if (next.access && !AuthSharedService.isAuthorized(next.access.authorizedRoles)) {
            event.preventDefault();
            $rootScope.$broadcast("event:auth-forbidden", {});
        }
    });

    // Call when the the client is confirmed
    $rootScope.$on('event:auth-loginConfirmed', function (event, data) {
        $rootScope.loadingAccount = false;
        var nextLocation = ($location.path() === '/login' ? "/" : $location.path());
        var delay = 10;

        $timeout(function () {
            sessionService.create(data);
            $rootScope.account = sessionService;
            $rootScope.authenticated = true;
            if (nextLocation == "/") {
                if (sessionService.isAdmin()) nextLocation = "/admin";
                else if (sessionService.isAdopt()) nextLocation = "/adopt";
                else if (sessionService.isPublisher()) nextLocation = "/publisher";
            }
            $location.path(nextLocation).replace();
        }, delay);

    });

    // Call when the 401 response is returned by the server
    $rootScope.$on('event:auth-loginRequired', function (event, data) {
        if ($rootScope.loadingAccount && data.status !== 401) {
            $rootScope.requestedUrl = $location.path();
            $location.path('/loading');
        } else {
            sessionService.invalidate();
            $rootScope.authenticated = false;
            $rootScope.loadingAccount = false;
            $location.path('/login');
        }
    });

    // Call when the 403 response is returned by the server
    $rootScope.$on('event:auth-forbidden', function (rejection) {
        $rootScope.$evalAsync(function () {
            $location.path('/error/403').replace();
        });
    });

    // Call when the user logs out
    $rootScope.$on('event:auth-loginCancelled', function () {
        $location.path('/login').replace();
    });

    // Get already authenticated user account
    authSharedService.getAccount();
});
