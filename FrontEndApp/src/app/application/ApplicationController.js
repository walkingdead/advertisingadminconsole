angular.module('advertisingAdminConsole')
    .controller('applicationController', ['$state', '$scope', '$rootScope', 'applicationService', 'commonUserService', 'APP_TYPES', 'CONTENT_TYPES',
        function ($state, $scope, $rootScope, applicationService, commonUserService, APP_TYPES, CONTENT_TYPES) {

            $scope.appTypes = APP_TYPES;
            $scope.contentTypeOptions = CONTENT_TYPES;

            $scope.contentTypeSettings = {
                scrollableHeight: '200px',
                scrollable: true,
                enableSearch: true
            };

            $scope.user = {};

            $scope.applications = [];
            $scope.model = {"applications": $scope.applications, selected: {}};

            $scope.application = {};

            $scope.createApplication = function (application) {
                var contentTypes = application.contentTypes.map(function (elem) {
                    return elem.id;
                });
                application.contentTypes = contentTypes;
                applicationService.createApplication($scope.user.id, application)
                    .then(function (data) {
                        $scope.application = {};
                        data.contentTypes = contentTypes;
                        $scope.model.applications.unshift(data);
                    }).catch(function (error) {
                });

            };

            $scope.deleteApplication = function (applicationId, $index) {
                applicationService.deleteApplication(applicationId)
                    .then(function (data) {
                        $scope.model.applications.splice($index, 1);
                    }).catch(function (error) {
                });
            };

            $scope.updateApplication = function (index) {
                var application = angular.copy($scope.model.selected);
                var contentTypes = application.contentTypes.map(function (elem) {
                    return elem.id;
                });
                application.contentTypes = contentTypes;
                applicationService.updateApplication($scope.user.id, application)
                    .then(function () {
                        $scope.model.selected.contentTypes = contentTypes;
                        $scope.model.applications[index] = angular.copy($scope.model.selected);
                        $scope.reset();
                    }).catch(function (error) {
                });
            };

            $scope.getUserById = function (userId) {
                commonUserService.getUserById(userId)
                    .then(function (data) {
                        $scope.user = data;
                        $scope.model.applications = data.applications;
                    }).catch(function (error) {
                });
            };

            $scope.getTemplate = function (application) {
                if (application.id !== undefined && application.id === $scope.model.selected.id) return 'edit';
                else return 'display';
            };

            $scope.editApplication = function (application) {
                var contentTypes = application.contentTypes.map(function (elem) {
                    return {'id': elem};
                });
                $scope.model.selected = angular.copy(application);
                $scope.model.selected.contentTypes = contentTypes;
            };

            $scope.reset = function () {
                $scope.model.selected = {};
            };

            $scope.getUserById($rootScope.user.id);

        }]);
