angular.module('advertisingAdminConsole')
    .service('applicationService', ['$http', '$q', 'commonUserService', 'CONFIG', function ($http, $q, commonUserService, CONFIG) {
        var that = this;

        that.createApplication = function (userId, application) {
            return $http.post(CONFIG.baseUrl + "app/user/" + userId,
                JSON.stringify(application)).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };

        that.deleteApplication = function (applicationId) {

            return $http.delete(CONFIG.baseUrl + "app/" + applicationId)
                .then(function (response) {
                        return response.data;
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                );
        };

        that.updateApplication = function (userId, application) {
            return $http.put(CONFIG.baseUrl + "app/user/" + userId,
                JSON.stringify(application)).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };
    }]);