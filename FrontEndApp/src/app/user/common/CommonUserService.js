angular.module('advertisingAdminConsole')
    .service('commonUserService', ['$http', '$q', 'CONFIG', function ($http, $q, CONFIG) {

        var that = this;

        that.createUser = function (user) {
            var authority = user.authorities;
            user.authorities = [user.authorities];
            return $http.post(CONFIG.baseUrl + "users/" + authority.toLowerCase(),
                JSON.stringify(user)).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };

        that.updateUser = function (user) {
            user.password = '**********';
            return $http.put(CONFIG.baseUrl + "users/" + user.authorities[0].name.toLowerCase(),
                 JSON.stringify(user)).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };

        that.deleteUser = function (userId, authority) {

            return $http.delete(CONFIG.baseUrl + "users/" + authority.toLowerCase() + "/" + userId,
                {}).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };

        that.getUserById = function (userId) {
            return $http.get(CONFIG.baseUrl + "users/user/" + userId)
                .then(function (response) {
                        return response.data;
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                );
        };
    }]);
