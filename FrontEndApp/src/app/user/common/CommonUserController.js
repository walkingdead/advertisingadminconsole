angular.module('advertisingAdminConsole')
    .controller('commonUserController', ['$scope', 'commonUserService', 'USER_ROLES', 'users', '$state',
        function ($scope, commonUserService, USER_ROLES, users, $state) {

        $scope.authorities = $state.current.authorities;

        $scope.users = users;
        $scope.model = {"users": $scope.users, selected: {}};

        $scope.user = {};

        $scope.createUser = function (user) {
            commonUserService.createUser(user)
                .then(function (data) {
                    $scope.model.users.unshift(data);
                    $scope.user = {};
                }).catch(function (error) {
            });

        };

        $scope.deleteUser = function (userId, authority, $index) {
            commonUserService.deleteUser(userId, authority)
                .then(function (data) {
                    $scope.model.users.splice($index, 1);
                }).catch(function (error) {
            });
        };

        $scope.updateUser = function (index) {
            var user = $scope.model.selected;
            commonUserService.updateUser(user).then(function () {
                $scope.model.users[index] = angular.copy($scope.model.selected);
                $scope.reset();
            }).catch(function (error) {
            });
        };

        $scope.getTemplate = function (user) {
            if (user.id === $scope.model.selected.id) return 'edit';
            else return 'display';
        };

        $scope.editUser = function (user) {
            $scope.model.selected = angular.copy(user);
        };

        $scope.reset = function () {
            $scope.model.selected = {};
        };
    }]);
