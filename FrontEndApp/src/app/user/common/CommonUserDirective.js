angular.module('advertisingAdminConsole')
    .directive('createUser', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/html/user-tmpl.html',
            scope: {
                authoritiesRaw: '@authorities',
                saveUser: '&createuser'
            },
            link: function (scope, element, attrs) {
                scope.authorities = scope.$eval(scope.authoritiesRaw);
            },
            controller: function ($scope) {
                $scope.createUser = function (user) {
                    $scope.userForm.$setPristine();
                    $scope.saveUser({user: user});
                };
            }
        };
    });
