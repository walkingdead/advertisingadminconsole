angular.module('advertisingAdminConsole')
    .directive('createApp', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/html/app-tmpl.html',
            scope: {
                appTypeRaw: '@apptypes',
                contentTypeRaw: '@contenttypes',
                saveApp: '&createapp'
            },
            link: function (scope, element, attrs) {
                scope.appTypes = scope.$eval(scope.appTypeRaw);
                scope.contentTypeOptions = scope.$eval(scope.contentTypeRaw);
                scope.application = {};
                scope.application.contentTypes = [];
            },
            controller: function ($scope) {
                $scope.createApp = function (application) {
                    $scope.applicationForm.$setPristine();
                    $scope.saveApp({application: application});
                };
            }
        };
    });
