angular.module('advertisingAdminConsole')
    .controller('adoptMainController', ['$state' ,'$scope',  function ($state, $scope) {

        $scope.initialise = function () {
            $scope.go = function (state) {
                $state.go(state);
            };

            $scope.tabData = [
                {
                    heading: 'Publishers',
                    route: 'adopt.publishers'
                },
                {
                    heading: 'Applications',
                    route: 'adopt.applications'
                }
            ];
            if ($state.current.name === "adopt") {
                $scope.go('adopt.publishers');
            }
        };
        $scope.initialise();
    }]);
