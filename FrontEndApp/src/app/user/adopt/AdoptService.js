angular.module('advertisingAdminConsole')
    .service('adoptService',[ '$http','$q', 'CONFIG', function ($http, $q, CONFIG) {
        var that = this;

        that.getUsersWithAuthority = function (authority) {

                return $http.get(CONFIG.baseUrl + "users/authority/" + authority,
                    {}).then(
                    function (response) {
                        return response.data;
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                );
            };
    }]);