angular.module('advertisingAdminConsole')
    .service('adminService', ['$http', '$q', 'CONFIG', function ($http, $q, CONFIG) {
        var that = this;

        that.getNonAdminUsers = function () {

            return $http.get(CONFIG.baseUrl + "users/nonadmins/").then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    return $q.reject(response);
                }
            );
        };

    }]);