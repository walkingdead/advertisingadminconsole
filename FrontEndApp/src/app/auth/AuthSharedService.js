angular.module('advertisingAdminConsole')
    .service('authSharedService', ['$rootScope', '$http', 'authService', 'sessionService', 'CONFIG',
        function ($rootScope, $http, authService, sessionService, CONFIG) {
            return {
                login: function (userName, password) {
                    var config = {
                        ignoreAuthModule: 'ignoreAuthModule',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    };
                    var data = "username=" + userName + "&password="+ password;
                    $http.post(CONFIG.baseUrl + 'authenticate', data, config)
                        .success(function (data) {
                            authService.loginConfirmed(data);
                        })
                        .error(function (data) {
                            $rootScope.authenticationError = true;
                            sessionService.invalidate();
                        });
                },
                getAccount: function () {
                    $rootScope.loadingAccount = true;
                    $http.get(CONFIG.baseUrl + 'security/account')
                        .then(function (response) {
                            authService.loginConfirmed(response.data);
                            $rootScope.userLogin = response.data.login;
                            $rootScope.user = response.data;
                        });
                },
                isAuthorized: function (authorizedRoles) {
                    if (!angular.isArray(authorizedRoles)) {
                        if (authorizedRoles == '*') {
                            return true;
                        }
                        authorizedRoles = [authorizedRoles];
                    }
                    var isAuthorized = false;
                    angular.forEach(authorizedRoles, function (authorizedRole) {
                        var authorized = (!!sessionService.login &&
                        sessionService.userRoles.indexOf(authorizedRole) !== -1);
                        if (authorized || authorizedRole == '*') {
                            isAuthorized = true;
                        }
                    });
                    return isAuthorized;
                },
                logout: function () {
                    $rootScope.authenticationError = false;
                    $rootScope.authenticated = false;
                    $rootScope.account = null;
                    $http.get(CONFIG.baseUrl + 'logout');
                    sessionService.invalidate();
                    authService.loginCancelled();
                }
            };
        }]);