angular.module('advertisingAdminConsole')
    .service('sessionService', function () {
        this.create = function (data) {
            this.id = data.id;
            this.login = data.login;
            this.userRoles = [];
            angular.forEach(data.authorities, function (value, key) {
                this.push(value.name);
            }, this.userRoles);
        };
        this.invalidate = function () {
            this.id = null;
            this.login = null;
            this.userRoles = null;
        };
        this.isAdmin = function () {
          return this.userRoles.indexOf('ADMIN') !== -1;
        };
        this.isAdopt = function () {
            return this.userRoles.indexOf('ADOPT') !== -1;
        };
        this.isPublisher = function () {
            return this.userRoles.indexOf('PUBLISHER') !== -1;
        };
        return this;

    });
