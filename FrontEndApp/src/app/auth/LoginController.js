angular.module('advertisingAdminConsole')
    .controller('loginController', ['$rootScope', '$scope', 'authSharedService',
        function ($rootScope, $scope, authSharedService) {
            $scope.login = function () {
                $rootScope.authenticationError = false;
                authSharedService.login(
                    $scope.username,
                    $scope.password
                );
            };
        }]);