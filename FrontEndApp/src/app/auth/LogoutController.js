angular.module('advertisingAdminConsole')
    .controller('logoutController', ['authSharedService',
        function (authSharedService) {
            authSharedService.logout();
        }]);