Assume that Node JS and npm already installed on your system.

Go To Directory `project-name/FrontEndApp`

Install bower
    npm install bower

Install bower dependencies
    bower install

Install dependencies:
    npm install

Install grunt
    npm install grunt --save-dev
    npm install -g grunt-cli --save-dev
    
Run project:
    grunt up
    
After go to localhost:9999

Accounts:

    Authorities: ADMIN
    login: admin@gmail.com
    password : admin
    
    Authorities: ADOPT
    login: adopt@gmail.com
    password: 12345
    
To launch backend part simply launch class : 
`com.advertising.console.AdvertisingAdminConsoleApplication`

To build project :
`mvn clean install`
    
